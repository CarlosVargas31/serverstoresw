package main;

import model.Lot;

import service.LotService;
import service.ProductService;
import service.WarehouseService;

public class Class1 {
    
    private ProductService productService;
    
    private WarehouseService warehouseService;
    
    private LotService lotService;
    
    public Class1() {
        productService = ProductService.getInstance();
        warehouseService = WarehouseService.getInstance();

        try {
            warehouseService.getAllWarehouse()
            .stream().forEachOrdered(product -> System.out.println(product));
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Class1 class1 = new Class1();
    }
    
}
