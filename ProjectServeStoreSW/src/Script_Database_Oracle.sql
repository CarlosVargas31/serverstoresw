/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  carlos
 * Created: 18/08/2020
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  carlos
 * Created: 18/08/2020
 */

DROP TABLE PRODUCTS CASCADE CONSTRAINTS;
DROP TABLE WAREHOUSES CASCADE CONSTRAINTS;
DROP TABLE LOTS CASCADE CONSTRAINTS;

CREATE TABLE "STORE"."PRODUCTS"
(
    CODE NUMBER(10) NOT NULL,
    NAME VARCHAR2(40) NOT NULL,
    PRICE NUMBER(19,2) NOT NULL,
    CONSTRAINT pk_products PRIMARY KEY (CODE),
    CONSTRAINT uk_products_name UNIQUE (NAME)
);

CREATE TABLE "STORE"."WAREHOUSES"
(
    ID NUMBER(10),
    NAME VARCHAR2(200) NOT NULL,
    ADDRESS VARCHAR2(300) NOT NULL,
    CONSTRAINT pk_warehouses PRIMARY KEY (ID),
    CONSTRAINT uk_warehouses_name UNIQUE (NAME),
    CONSTRAINT uk_warehouses_address UNIQUE (ADDRESS)
);

CREATE TABLE "STORE"."LOTS"
(
    ID NUMBER(32) NOT NULL,
    PRODUCT_CODE NUMBER(10) NOT NULL,
    WAREHOUSE_ID NUMBER(10) NOT NULL,
    STOCK NUMBER(10) NOT NULL,
    CONSTRAINT pk_lots PRIMARY KEY (ID),
    CONSTRAINT fk_lots_products FOREIGN KEY (PRODUCT_CODE)
        REFERENCES "STORE"."PRODUCTS" (CODE)
        ON DELETE SET NULL,
    CONSTRAINT fk_lots_warehouses FOREIGN KEY (WAREHOUSE_ID)
        REFERENCES STORE.WAREHOUSES (ID)
        ON DELETE CASCADE
);

INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (1, 'Exito Apartado', 'Calle 103 con carrera 100 Centro Comercial Plaza del Rio');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (2, 'Exito Itagui', 'Carrera 50 A # 41 - 42');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (3, 'Exito Indiana', 'Kilometro 17 via palmas local 101 mall indiana');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (4, 'Exito Mayorca', 'Calle 44 # 50 Sur 45 Locales 1110 - 1111 - 2207');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (5, 'Exito Girardot', 'Avenida 40 Calle Septima # 45 - 185 La Esperanza');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (6, 'Supertienda Olimpica La Paz', 'Calle 108 No.12F - 18');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (7, 'Supertienda Olimpica Las Nieves', 'Calle 24 con Carrera 11 esq');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (8, 'Sao 53', 'Calle 53 No.46-38 CC Portal del Prado');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (9, 'Jumbo Chipichape', 'Calle 40 Norte # 6A - 45');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (10, 'Super Inter Calle 37', 'Carrera 5 Calle 37 esq');
INSERT INTO "STORE"."WAREHOUSES"(id, name, address) 
	VALUES (11, 'Super Inter Honda', 'Carrera 11 #16-48');

INSERT INTO "STORE"."PRODUCTS"(code, name, price) VALUES (10001, 'Ham', 3500);
INSERT INTO "STORE"."PRODUCTS"(code, name, price) VALUES (10002, 'Yogurt', 4000);
INSERT INTO "STORE"."PRODUCTS"(code, name, price) VALUES (10003, 'Butter', 1100);
INSERT INTO "STORE"."PRODUCTS"(code, name, price) VALUES (10004, 'Milk', 2500);
INSERT INTO "STORE"."PRODUCTS"(code, name, price) VALUES (10005, 'Cheese', 3000);