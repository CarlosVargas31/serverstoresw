package serviceWeb;

import java.util.List;

import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import model.LotDTO;

import service.LotService;

@WebService(serviceName = "LotServiceSW")
public class LotSW {
    
    private LotService lotService;
    
    public LotSW() {
        this.lotService = LotService.getInstance();
    }

    @WebMethod
    public List<LotDTO> getAllLots() {
        return lotService.getAllLots();
    }

    @WebMethod
    public List<LotDTO> findByStockLikeLots(@WebParam(name = "arg0") Integer stock) throws Exception {
        return lotService.findByStockLikeLots(stock);
    }

    @WebMethod
    public LotDTO findLot(@WebParam(name = "arg0") Long id) throws Exception {
        return lotService.findLot(id);
    }

    @WebMethod
    @Oneway
    public void saveLot(@WebParam(name = "arg0") LotDTO lot, @WebParam(name = "arg1") Integer code,
                        @WebParam(name = "arg2") Integer id) {
        lotService.saveLot(lot, code, id);
    }

    @WebMethod
    public void updateLot(@WebParam(name = "arg0") Long id, @WebParam(name = "arg1") LotDTO lot) throws Exception {
        lotService.updateLot(id, lot);
    }

    @WebMethod
    public void removeLot(@WebParam(name = "arg0") Long id) throws Exception {
        lotService.removeLot(id);
    }
    
}
