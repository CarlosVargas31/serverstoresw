package serviceWeb;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import model.LotDTO;
import model.WarehouseDTO;
import service.WarehouseService;

@WebService(serviceName = "WarehouseServiceSW")
public class WarehouseSW {
    
    private WarehouseService warehouseService;
    
    public WarehouseSW() {
        this.warehouseService = WarehouseService.getInstance();
    }
    
    @WebMethod
    public List<WarehouseDTO> getAllWarehouse() {
        return warehouseService.getAllWarehouse();
    }

    @WebMethod
    public List<LotDTO> findLotsWarehouse(@WebParam(name = "arg0") Integer id) throws Exception {
        return warehouseService.findLotsWarehouse(id);
    }

    @WebMethod
    public WarehouseDTO findWarehouses(@WebParam(name = "arg0") Integer id) throws Exception {
        return warehouseService.findWarehouses(id);
    }    
    
}
