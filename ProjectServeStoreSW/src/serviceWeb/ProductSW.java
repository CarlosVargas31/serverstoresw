package serviceWeb;

import java.util.List;

import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import model.LotDTO;
import model.ProductDTO;

import service.ProductService;

@WebService(serviceName = "ProductServerServiceSW", portName = "ProductServerSWPort")
public class ProductSW {
    
    private ProductService productService;
    
    public ProductSW() {
        this.productService = ProductService.getInstance();
    }

    @WebMethod
    public List<ProductDTO> getAllProducts() {
        return productService.getAllProducts();
    }

    @WebMethod
    public List<ProductDTO> getAllByNameLikeProducts(@WebParam(name = "arg0") String name) throws Exception {
        return productService.getAllByNameLikeProducts(name);
    }

    @WebMethod
    public List<LotDTO> findLotsProduct(@WebParam(name = "arg0") Integer code) throws Exception {
        return productService.findLotsProduct(code);
    }

    @WebMethod
    public ProductDTO findProduct(@WebParam(name = "arg0") Integer code) throws Exception {
        return productService.findProduct(code);
    }

    @WebMethod
    @Oneway
    public void saveProduct(@WebParam(name = "arg0") ProductDTO product) {
        productService.saveProduct(product);
    }

    @WebMethod
    public void updateProduct(@WebParam(name = "arg0") Integer code,
                              @WebParam(name = "arg1") ProductDTO product) throws Exception {
        productService.updateProduct(code, product);
    }

    @WebMethod
    public void removeProduct(@WebParam(name = "arg0") Integer code) throws Exception {
        productService.removeProduct(code);
    }
    
}
