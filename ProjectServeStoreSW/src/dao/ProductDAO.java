package dao;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.persistence.criteria.CriteriaBuilder;

import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Root;

import model.Lot;
import model.Product;

public class ProductDAO {
    
    private EntityManager em;
        
    private final Class entityClass;
            
    public ProductDAO() {
        entityClass = Product.class;
    }
        
    public EntityManager getEntityManager() {
        return em;
    }
        
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public void save(Product product) throws EntityExistsException {
        try {
            getEntityManager().getTransaction().begin();
            getEntityManager().persist(product);
            getEntityManager().getTransaction( ).commit( );
        } catch (RollbackException e) {
            throw new EntityExistsException(
                    "The product already exists... "
                    + "Remember that the name and code of the product are exclusive"
            );
        }   
    }

    public void update(Integer code, Product product) throws Exception {
        Product productToUpdate = find(code);
        productToUpdate.setName(product.getName());
        productToUpdate.setPrice(product.getPrice());
        getEntityManager().getTransaction().begin();
        getEntityManager().merge(productToUpdate);
        getEntityManager().getTransaction( ).commit( );
    }

    public void remove(Integer code) throws Exception {
        Product productToDelete = find(code);
        getEntityManager().getTransaction().begin();
        getEntityManager().remove(getEntityManager().merge(productToDelete));
        getEntityManager().getTransaction( ).commit( );
    }

    public Product find(Integer code) throws Exception {
        Product product = (Product) getEntityManager().find(entityClass, code);
        
        if (product == null) {
            throw new Exception("Product not found..");
        }
        
        return product;
    }
        
    public List<Lot> findLots(Integer code) throws Exception {
        Product product = find(code);
        
        if (product.getLots().isEmpty()) {
            throw new Exception("There aren't lots for this product");
        }
        
        return product.getLots();
    } 
        
    public List<Product> findByNameLike(String name) throws Exception {
        String query = "SELECT * FROM PRODUCTS WHERE NAME LIKE '%" + name + "%'";
        Query q = getEntityManager().createNativeQuery(query, entityClass);
        List<Product> result = q.getResultList();
        
        if (result.isEmpty()) {
            throw new Exception("No product was found by the filtered parameter");
        }
            
        return result;
    }

    public List<Product> findAll() {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Product> cq = cb.createQuery(entityClass);
        Root<Product> root = cq.from(entityClass);
        cq.select(root).orderBy(cb.asc(root.get("code")));
        return getEntityManager().createQuery(cq).getResultList();
    }
        
}
