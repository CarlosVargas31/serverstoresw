package dao;

import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.criteria.CriteriaBuilder;

import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Root;

import model.Lot;
import model.Warehouse;

public class WarehouseDAO {
    
    private EntityManager em;
        
        private final Class entityClass;
            
        public WarehouseDAO() {
            entityClass = Warehouse.class;
        }
        
        public EntityManager getEntityManager() {
            return em;
        }
        
        public void setEntityManager(EntityManager em) {
            this.em = em;
        }
        
        public Warehouse find(Integer id) throws Exception {
            Warehouse warehouse = (Warehouse) getEntityManager().find(entityClass, id);
            
            if (warehouse == null) {
                throw new Exception("Warehouse not found..");
            }
            
            return warehouse;
        }
        
        public List<Lot> findLots(Integer id) throws Exception {
            Warehouse warehouse = find(id);
            
            if (warehouse.getLots().isEmpty()) {
                throw new Exception("There aren't lots for this warehouse");
            }
            
            return warehouse.getLots();
        }
        
        public List<Warehouse> findAll() {
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Warehouse> cq = cb.createQuery(entityClass);
            Root<Warehouse> root = cq.from(entityClass);
            cq.select(root).orderBy(cb.asc(root.get("id")));
            return getEntityManager().createQuery(cq).getResultList();
        }
    
}
